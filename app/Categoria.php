<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    // Relación uno a muchos
    public function productos(){
        return $this->hasMany('App\Producto');
    }
}
