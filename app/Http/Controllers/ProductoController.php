<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Producto;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = DB::table('productos as P')
            ->select('P.id', 'P.marca_id', 'P.categoria_id', 'P.proveedor_id' ,'P.codigo', 'P.nombre', 'M.nombre as marca', 'C.nombre as categoria', 'PR.nombre as proveedor', 'P.precio_proveedor', 'P.precio_venta', 'P.cantidad')
            ->join('marcas as M', 'P.marca_id', 'M.id')
            ->join('categorias as C', 'P.categoria_id', 'C.id')
            ->join('proveedores as PR', 'P.proveedor_id', 'PR.id')
            ->orderBy('P.nombre', 'asc')
            ->get();

        return $productos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Recoger datos
        $codigo = $request->input('codigo');
        $nombre = $request->input('nombre');
        $marca_id = $request->input('marca');
        $categoria_id = $request->input('categoria');
        $proveedor_id = $request->input('proveedor');
        $precio_proveedor = $request->input('precio_proveedor');
        $precio_venta = $request->input('precio_venta');
        $cantidad = $request->input('cantidad');

        // Obtener cantidad de productos que tengan el mismo nombre y marca
        $cant_productos = Producto::where([
                                    ['nombre', '=', $nombre],
                                    ['marca_id', '=', $marca_id]
                                ])->count();

        // Todo bien; no existe un producto con el mismo nombre y marca
        if($cant_productos == 0){
            // Asignar valores a objeto
            $producto = new Producto();
            $producto->codigo = $codigo;
            $producto->marca_id = $marca_id;
            $producto->categoria_id = $categoria_id;
            $producto->proveedor_id = $proveedor_id;
            $producto->nombre = $nombre;
            $producto->precio_proveedor = $precio_proveedor;
            $producto->precio_venta = $precio_venta;
            $producto->cantidad = $cantidad;

            try {
                // Guardar objeto
                $producto->save();
                $respuesta = true;
                $mensaje = 'El producto ' . $producto->nombre . ' con marca ' . $producto->marca->nombre . ' ha sido agregado correctamente.';
            } catch (\Illuminate\Database\QueryException $e) {
                $respuesta = false;
                $mensaje = 'Error! El código ingresado ya posee un producto asociado.';
            }
        } else {
            // Existen productos con el mismo nombre y marca
            $respuesta = false;
            $mensaje = 'Error! Ya existe un producto con el mismo nombre y marca.';
        }

        $datos = Array(
            'ok' => $respuesta,
            'mensaje' => $mensaje
        );

        if($respuesta){
            $detalle = Array(
                'id' => $producto->id,
                'marca_id' => $producto->marca_id,
                'categoria_id' => $producto->categoria_id,
                'proveedor_id' => $producto->proveedor_id,
                'codigo' => $producto->codigo,
                'nombre' => $producto->nombre,
                'marca' => $producto->marca->nombre,
                'categoria' => $producto->categoria->nombre,
                'proveedor' => $producto->proveedor->nombre,
                'precio_proveedor' => $producto->precio_proveedor,
                'precio_venta' => $producto->precio_venta,
                'cantidad' => $producto->cantidad
            );

            $datos['detalle'] = $detalle;
        }

        return response()->json($datos);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producto = Producto::find($id);

        // Recoger datos
        $nombre = $request->input('nombre');
        $marca_id = $request->input('marca');
        $categoria_id = $request->input('categoria');
        $proveedor_id = $request->input('proveedor');
        $precio_proveedor = $request->input('precio_proveedor');
        $precio_venta = $request->input('precio_venta');
        $cantidad = $request->input('cantidad');

        // Obtener cantidad de productos que tengan el mismo nombre y marca
        $cant_productos = Producto::where([
                                    ['nombre', '=', $nombre],
                                    ['marca_id', '=', $marca_id],
                                    ['id', '<>', $id]
                                ])
                                ->count();  

        // Todo bien; no existe un producto con el mismo nombre y marca que no sea el producto a editar
        if($cant_productos == 0){
            // Asignar nuevos datos
            $producto->marca_id = $marca_id;
            $producto->categoria_id = $categoria_id;
            $producto->proveedor_id = $proveedor_id;
            $producto->nombre = $nombre;
            $producto->precio_proveedor = $precio_proveedor;
            $producto->precio_venta = $precio_venta;
            $producto->cantidad = $cantidad;

            // Guardar objeto
            $producto->save();
            $respuesta = true;
            $mensaje = 'El producto ha sido editado correctamente.';
        } else {
            // Existen productos con el mismo nombre y marca
            $respuesta = false;
            $mensaje = 'Error! Ya existe un producto con el mismo nombre y marca.';
        }

        $datos = Array(
            'ok' => $respuesta,
            'mensaje' => $mensaje
        );

        if($respuesta){
            $detalle = Array(
                'id' => $producto->id,
                'marca_id' => $producto->marca_id,
                'categoria_id' => $producto->categoria_id,
                'proveedor_id' => $producto->proveedor_id,
                'codigo' => $producto->codigo,
                'nombre' => $producto->nombre,
                'marca' => $producto->marca->nombre,
                'categoria' => $producto->categoria->nombre,
                'proveedor' => $producto->proveedor->nombre,
                'precio_proveedor' => $producto->precio_proveedor,
                'precio_venta' => $producto->precio_venta,
                'cantidad' => $producto->cantidad
            );

            $datos['detalle'] = $detalle;
        }

        return response()->json($datos);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = Producto::find($id);

        $producto->delete();
        $respuesta = true;

        return response()->json([
            'nombre' => $producto->nombre,
            'marca' => $producto->marca->nombre
        ]);
    }
}
