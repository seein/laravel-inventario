<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    // Relación uno a muchos (inverso)
    public function categoria(){
        return $this->belongsTo('App\Categoria');
    }

    // Relación uno a muchos (inverso)
    public function marca(){
        return $this->belongsTo('App\Marca');
    }

    // Relación uno a muchos (inverso)
    public function proveedor(){
        return $this->belongsTo('App\Proveedor');
    }
}
