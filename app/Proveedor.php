<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
	protected $table = 'proveedores';

    // Relación uno a muchos
    public function productos(){
        return $this->hasMany('App\Producto');
    }
}
