-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-04-2020 a las 22:46:13
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inventario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'LIBRERÍA - BAZAR', '2019-12-24 17:17:22', '2019-12-24 17:17:22'),
(2, 'CONFITES', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(3, 'PERFUMERÍA Y BELLEZA', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(4, 'JUGUETES Y REGALO', '2019-12-24 17:17:23', '2019-12-24 17:17:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE `marcas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'FULTONS', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(2, 'SELLOFFICE', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(3, 'ART & CRAFT', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(4, 'PROARTE', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(5, 'PATELWELL', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(6, 'FIXO', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(7, 'ARTEL', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(8, 'JM', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(9, 'MEILLER', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(10, 'ISOFIT', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(11, 'LEWER', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(12, 'ANCA', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(13, 'NILSA', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(14, 'HALLEY', '2019-12-24 17:17:23', '2019-12-24 17:17:23'),
(15, 'TORRE', '2019-12-24 17:17:24', '2019-12-24 17:17:24'),
(16, 'PARADONES', '2019-12-24 17:17:24', '2019-12-24 17:17:24'),
(17, 'FOSCA', '2019-12-24 17:17:24', '2019-12-24 17:17:24'),
(18, 'ARON', '2019-12-24 17:17:24', '2019-12-24 17:17:24'),
(19, 'HAND', '2019-12-24 17:17:24', '2019-12-24 17:17:24'),
(20, 'KADIO', '2019-12-24 17:17:24', '2019-12-24 17:17:24'),
(21, 'KENKO', '2019-12-24 17:17:24', '2019-12-24 17:17:24'),
(22, 'ROKA', '2019-12-24 17:17:24', '2019-12-24 17:17:24'),
(23, 'PAPER MATE', '2019-12-24 17:17:24', '2019-12-24 17:17:24'),
(24, 'BIC', '2019-12-24 17:17:24', '2019-12-24 17:17:24'),
(25, 'SONY', '2019-12-24 17:17:24', '2019-12-24 17:17:24'),
(26, 'MONAMI', '2019-12-24 17:17:25', '2019-12-24 17:17:25'),
(27, 'SABONIS', '2019-12-24 17:17:25', '2019-12-24 17:17:25'),
(28, 'SHARPIE', '2019-12-24 17:17:25', '2019-12-24 17:17:25'),
(29, 'BALLERINA', '2019-12-24 17:17:25', '2019-12-24 17:17:25'),
(30, 'KINGSTON', '2019-12-24 17:17:25', '2019-12-24 17:17:25'),
(31, 'AVON', '2019-12-24 17:17:25', '2019-12-24 17:17:25'),
(32, 'CADENA', '2019-12-24 17:17:25', '2019-12-24 17:17:25'),
(33, 'KODAK', '2019-12-24 17:17:25', '2019-12-24 17:17:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_11_29_232816_create_table_marcas', 1),
(4, '2019_11_29_232918_create_table_categorias', 1),
(5, '2019_11_29_232945_create_table_proveedores', 1),
(6, '2019_11_29_232955_create_table_productos', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(10) UNSIGNED NOT NULL,
  `codigo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marca_id` int(10) UNSIGNED NOT NULL,
  `categoria_id` int(10) UNSIGNED NOT NULL,
  `proveedor_id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio_proveedor` int(11) NOT NULL,
  `precio_venta` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `codigo`, `marca_id`, `categoria_id`, `proveedor_id`, `nombre`, `precio_proveedor`, `precio_venta`, `cantidad`, `created_at`, `updated_at`) VALUES
(5, '12312', 24, 1, 1, 'GOMA DE BORRAR', 300, 500, 10, '2019-12-24 18:14:51', '2019-12-24 19:48:04'),
(12, '12345', 4, 1, 3, 'LÁPIZ PASTA', 200, 400, 5, '2019-12-24 19:49:16', '2019-12-24 19:49:16'),
(14, '54321', 23, 1, 1, 'SILICONA MEDIANA', 300, 600, 8, '2019-12-24 19:50:16', '2019-12-24 20:53:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'ALESAN', '2019-12-24 17:17:26', '2019-12-24 17:17:26'),
(2, 'BIOZAR', '2019-12-24 17:17:26', '2019-12-24 17:17:26'),
(3, 'OLIMPICA', '2019-12-24 17:17:26', '2019-12-24 17:17:26'),
(4, 'FRUNA', '2019-12-24 17:17:27', '2019-12-24 17:17:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categorias_nombre_unique` (`nombre`);

--
-- Indices de la tabla `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `marcas_nombre_unique` (`nombre`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `productos_codigo_unique` (`codigo`),
  ADD KEY `productos_marca_id_foreign` (`marca_id`),
  ADD KEY `productos_categoria_id_foreign` (`categoria_id`),
  ADD KEY `productos_proveedor_id_foreign` (`proveedor_id`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `proveedores_nombre_unique` (`nombre`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `marcas`
--
ALTER TABLE `marcas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_categoria_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`),
  ADD CONSTRAINT `productos_marca_id_foreign` FOREIGN KEY (`marca_id`) REFERENCES `marcas` (`id`),
  ADD CONSTRAINT `productos_proveedor_id_foreign` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedores` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
