# Inventario (Backend) - Laravel

### Pre-requisitos 📋

* PHP >= 7.1.3
* Composer
* MySql

### Instalación 🔧

```
Crear una base de datos con el nombre "inventario" e importar el archivo "inventario.sql" en la DB, luego configurar archivo .env con los datos correspondientes.
```

_En el directorio del proyecto ingresar los siguientes comandos a través de una terminal_

* composer install
* php artisan key:generate

_Si existen errores al momento de ejecutar "composer install" intenta ejecutar el siguiente comando_

* composer update

## Despliegue 📦

_Ejecutar el siguiente comando para correr el proyecto_

* php artisan serve

## Inventario (Frontend) - Angular🛠️

* [Ir al repositorio](https://gitlab.com/seein/angular-inventario)